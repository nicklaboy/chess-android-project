package com.example.android73;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.io.File;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, Serializable {

    /** The filename to save the state of the photos app to.*/
    public static final String currentGameFile = "currentGame";

    public static final int SORT_BY_FILENAME = 1;
    public static final int SORT_BY_MODIFIED = 2;

    public boolean savesToDelete() {
        File folder = new File(getFilesDir().getPath());
        File[] listOfFiles = folder.listFiles();
        final ArrayList<File> validfiles = new ArrayList<File>();
        ArrayList<CharSequence> validFileStrings = new ArrayList<CharSequence>();

        for(File currentFile : listOfFiles) {
            String fileName = currentFile.getName();
            if (fileName.contains(".") && fileName.substring(fileName.lastIndexOf(".")).equalsIgnoreCase(".dat")) {
                if(!fileName.equalsIgnoreCase(currentGameFile + ".dat")) {
                    validfiles.add(currentFile);
                }
            }
        }

        return validfiles.size() > 0;
    }

    public boolean inProgresssGameDoesntExist() {
        File file = new File(getFilesDir(), currentGameFile + ".dat");
        return !file.exists();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Button continueButton = findViewById(R.id.continueButton);
        Button deleteButton = findViewById(R.id.deleteSaveButton);
        Button replayButton = findViewById(R.id.replayButton);

        if(inProgresssGameDoesntExist()) {
            continueButton.setEnabled(false);
        } else {
            continueButton.setEnabled(true);
        }

        if(savesToDelete()) {
            deleteButton.setEnabled(true);
            replayButton.setEnabled(true);
        } else {
            deleteButton.setEnabled(false);
            replayButton.setEnabled(false);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (checkPermission()) {
            requestPermissionAndContinue();
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button newGameButton = findViewById(R.id.newGameButton);
        newGameButton.setOnClickListener(this);

        Button continueButton = findViewById(R.id.continueButton);
        continueButton.setOnClickListener(this);

        Button deleteButton = findViewById(R.id.deleteSaveButton);
        deleteButton.setOnClickListener(this);

        Button replayButton = findViewById(R.id.replayButton);
        replayButton.setOnClickListener(this);

        if(inProgresssGameDoesntExist()) {
            continueButton.setEnabled(false);
        } else {
            continueButton.setEnabled(true);
        }

        if(savesToDelete()) {
            deleteButton.setEnabled(true);
            replayButton.setEnabled(true);
        } else {
            deleteButton.setEnabled(false);
            replayButton.setEnabled(false);
        }

        Button exitButton = findViewById(R.id.exitButton);
        exitButton.setOnClickListener(this);
    }

    private static final int PERMISSION_REQUEST_CODE = 200;
    private boolean checkPermission() {

        return ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                ;
    }

    private void requestPermissionAndContinue() {
        if (ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, WRITE_EXTERNAL_STORAGE)
                    && ActivityCompat.shouldShowRequestPermissionRationale(this, READ_EXTERNAL_STORAGE)) {
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
                alertBuilder.setCancelable(true);
                alertBuilder.setTitle("Need read and write permissions to proceed...");
                alertBuilder.setMessage("Please allow read and write permissions to continue...");
                alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{WRITE_EXTERNAL_STORAGE
                                , READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
                    }
                });
                AlertDialog alert = alertBuilder.create();
                alert.show();
            } else {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{WRITE_EXTERNAL_STORAGE,
                        READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode != PERMISSION_REQUEST_CODE) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void loadFilesWithMode (final int sortMode) {
        File folder = new File(getFilesDir().getPath());
        File[] listOfFiles = folder.listFiles();
        final ArrayList<File> validfiles = new ArrayList<File>();
        ArrayList<CharSequence> validFileStrings = new ArrayList<CharSequence>();
        DateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

        for(File currentFile : listOfFiles) {
            String fileName = currentFile.getName();
            if (fileName.contains(".") && fileName.substring(fileName.lastIndexOf(".")).equalsIgnoreCase(".dat")) {
                if(!fileName.equalsIgnoreCase(currentGameFile + ".dat")) {
                    validfiles.add(currentFile);
                }
            }
        }

        validfiles.sort(new Comparator<File>() {
            @Override
            public int compare(File file, File t1) {
                if(sortMode == SORT_BY_FILENAME) {
                    return file.getName().compareTo(t1.getName());
                } else if(sortMode == SORT_BY_MODIFIED) {
                    return (new Date(file.lastModified()).compareTo(new Date(t1.lastModified())));
                } else {
                    return 0;
                }
            }
        });

        for(File currentFile : validfiles) {
            validFileStrings.add(String.format(currentFile.getName().substring(0, currentFile.getName().lastIndexOf(".dat")) + " on " + dateFormatter.format(new Date(currentFile.lastModified()))));
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Please choose a game to load...");
          builder.setItems(validFileStrings.toArray(new CharSequence[0]),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        System.out.println(validfiles.get(which).getName());
                        Intent intent = new Intent(MainActivity.this, ChessActivity.class);
                        intent.putExtra("mode", ChessActivity.REPLAY_MODE);
                        intent.putExtra("replayFile", validfiles.get(which).getName());
                        startActivity(intent);
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public void promptToDeleteGame() {
        File folder = new File(getFilesDir().getPath());
        File[] listOfFiles = folder.listFiles();
        final ArrayList<File> validfiles = new ArrayList<File>();
        ArrayList<CharSequence> validFileStrings = new ArrayList<CharSequence>();
        DateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

        for(File currentFile : listOfFiles) {
            String fileName = currentFile.getName();
            if (fileName.contains(".") && fileName.substring(fileName.lastIndexOf(".")).equalsIgnoreCase(".dat")) {
                if(!fileName.equalsIgnoreCase(currentGameFile + ".dat")) {
                    validfiles.add(currentFile);
                }
            }
        }

        validfiles.sort(new Comparator<File>() {
            @Override
            public int compare(File file, File t1) {
                return file.getName().compareTo(t1.getName());
            }
        });

        for(File currentFile : validfiles) {
            validFileStrings.add(String.format(currentFile.getName().substring(0, currentFile.getName().lastIndexOf(".dat")) + " on " + dateFormatter.format(new Date(currentFile.lastModified()))));
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Please choose a save to delete...");
        builder.setItems(validFileStrings.toArray(new CharSequence[0]),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        File dir = getFilesDir();
                        File file = new File(dir, validfiles.get(which).getName());
                        boolean deleted = file.delete();
                        if(!deleted) {
                            System.out.println("failed to delete save game...");
                        }

                        Button replayButton = findViewById(R.id.replayButton);
                        Button deleteButton = findViewById(R.id.deleteSaveButton);
                        if(savesToDelete()) {
                            deleteButton.setEnabled(true);
                            replayButton.setEnabled(true);
                        } else {
                            deleteButton.setEnabled(false);
                            replayButton.setEnabled(false);
                        }
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void promptForSortModeAndDisplaySaveList() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("How would you like to view your saved games?");
        builder.setItems(new CharSequence[]
                        {"Sorted by date", "Sorted by name", "Cancel"},
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == 0) {
                            loadFilesWithMode(SORT_BY_MODIFIED);
                        } else if (which == 1) {
                            loadFilesWithMode(SORT_BY_FILENAME);
                        } else if(which == 2) {
                            System.out.println("cancelled");
                        }
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    @Override
    public void onClick(View v) {

        if(!checkPermission()) {
            if(v.getId() == R.id.newGameButton) {
                System.out.println("new game");
                Intent intent = new Intent(this, ChessActivity.class);
                intent.putExtra("mode", ChessActivity.PLAY_MODE);
                startActivity(intent);
            }

            if(v.getId() == R.id.continueButton) {
                System.out.println("continue");
                Intent intent = new Intent(this, ChessActivity.class);
                intent.putExtra("mode", ChessActivity.PLAY_MODE);
                intent.putExtra("saveFile", currentGameFile + ".dat");
                startActivity(intent);
            }

            if(v.getId() == R.id.deleteSaveButton) {
                System.out.println("delete");
                promptToDeleteGame();
            }

            if(v.getId() == R.id.replayButton) {
                System.out.println("replay");
                promptForSortModeAndDisplaySaveList();
            }
        } else {
            if(v.getId() == R.id.newGameButton || v.getId() == R.id.continueButton || v.getId() == R.id.replayButton) {
                if (checkPermission()) {
                    requestPermissionAndContinue();
                }
            }
        }

        if(v.getId() == R.id.exitButton) {
            System.out.println("exit");
            System.exit(1);
        }
    }
}
