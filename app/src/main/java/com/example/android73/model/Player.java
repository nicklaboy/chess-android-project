package com.example.android73.model;

import com.example.android73.model.chessPieces.ChessPiece;
import com.example.android73.model.chessPieces.Pawn;
import com.example.android73.model.chessPieces.Rook;
import com.example.android73.model.chessPieces.Knight;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;

import com.example.android73.chess.Chess;
import com.example.android73.model.chessPieces.Bishop;
import com.example.android73.model.chessPieces.King;
import com.example.android73.model.chessPieces.Queen;

/**
 * The class that represents the Players themselves. This class contains
 * a collection of ChessPieces and a String used to identify the Player in the
 * console as members. It also contains methods that contain the logic used to 
 * carry out moves represented by the user's input and various utilities for checking 
 * for check, checkmate, and stalemate as well as to manipulate the collection of pieces.
 * This class also contains constructors used to create a Player using either a playerCode 
 * or another Player object (to duplicate the provided Player). 
 * 
 * @author Rob Holda 
 * @author Nick LaBoy
 */
public class Player {
	
	/** The playerCode associated with the White player. This is used in input validation
	 * and handling moves.*/
	public static String WHITE_CODE = "w";
	
	/** The playerCode associated with the Black player. This is used in input validation
	 * and handling moves.*/
	public static String BLACK_CODE = "b";
	
	/** The playerName associated with the White player.*/
	public static String whiteName = "White";
	
	/** The playerName associated with the White player.*/
	public static String blackName = "Black";
	
	/** The string used to identify the Player in output. This is used in reporting 
	 * the game state to the user (such as who's turn it is and who has won the game).*/
	public String playerName;

	public String playerCode;
	
	/** The collection of ChessPiece objects that belongs to the Player.
	 * <b>Note: ChessPieces in this collection <i>can</i> be null.</b> */
	public ArrayList<ChessPiece> myPieces;
	
	/**
	 * Initializes the Player based on the playerCode provided as either White or Black.
	 * 
	 * @param playerCode The playerCode string used to identify the Player we're creating.
	 * This will either be WHITE_CODE or BLACK_CODE.
	 */
	public Player(String playerCode) {
		myPieces = new ArrayList<ChessPiece>();
		
		if(playerCode == BLACK_CODE) {
			initializeAsBlack();
			this.playerCode = playerCode;
		} else {
			initializeAsWhite();
            this.playerCode = playerCode;
		}
	}
	
	/**
	 * Initializes the Player based on the provided Player, essentially creating a 
	 * duplicate of the Player object provided in the player argument and all its ChessPiece objects.
	 * 
	 * @param player The Player object to duplicate.
	 */
	public Player (Player player) {
		this.myPieces = new ArrayList<ChessPiece>();
		for(int pieceIndex = 0; pieceIndex < player.myPieces.size(); pieceIndex++) {
			ChessPiece currentPiece = player.myPieces.get(pieceIndex);
			
			if(currentPiece != null) {
				if(currentPiece instanceof Pawn) {
					this.myPieces.add(new Pawn((Pawn)currentPiece));
				} else if(currentPiece instanceof Rook) {
					this.myPieces.add(new Rook((Rook)currentPiece));
				} else if(currentPiece instanceof Knight) {
					this.myPieces.add(new Knight((Knight)currentPiece));
				} else if(currentPiece instanceof Bishop) {
					this.myPieces.add(new Bishop((Bishop)currentPiece));
				} else if(currentPiece instanceof Queen) {
					this.myPieces.add(new Queen((Queen)currentPiece));
				} else if(currentPiece instanceof King) {
					this.myPieces.add(new King((King)currentPiece));
				}				
			}
		}
	}
	
	/**
	 * Loops through the Player's myPieces collection and any Pawn objects that are not the ChessPiece
	 * provided in the lastPieceMoved parameter are updated so their canBeEnPassanted values are reset to false.
	 * 
	 * @param lastPieceMoved The last ChessPiece moved used to determine if the Pawn objects whose canBeEnPassanted
	 * values are being reset were the last ChessPiece to be moved and if so, they are not reset. 
	 */
	public void clearEnPassantablePieces(ChessPiece lastPieceMoved) {
		for(int currentPieceIndex = 0; currentPieceIndex < myPieces.size(); currentPieceIndex++) {
			ChessPiece currentPiece = myPieces.get(currentPieceIndex);
			if(lastPieceMoved != null && currentPiece != null && currentPiece instanceof Pawn) {
				if(!lastPieceMoved.equals(currentPiece)) {
					((Pawn)currentPiece).canBeEnPassanted = false;
				}
			}
		}
	}
	
	/**
	 * Loops through the provided opposing Player object's pieces and determines which of them is
	 * currently causing the Player to be in check, adding those pieces to an ArrayList of ChessPiece
	 * objects and returning that object.
	 * 
	 * @param them The opposing Player object from whom to build a collection of 
	 * ChessPieces causing check for the Player.
	 * 
	 * @return The ArrayList of ChessPieces belonging to the opponent Player that is causing 
	 * check for the Player.
	 */
	public ArrayList<ChessPiece> piecesCausingCheck(Player them) {
		ArrayList<ChessPiece> checkPieces = new ArrayList<ChessPiece>();
		for(int currentPieceIndex = 0; currentPieceIndex < them.myPieces.size(); currentPieceIndex++) {
			ChessPiece currentPiece = them.myPieces.get(currentPieceIndex);
			if(currentPiece != null) {
				if(currentPiece.canMoveToNewPosition(this.getMyKing().xPos, this.getMyKing().yPos, ' ', them, this)) {
					checkPieces.add(currentPiece);
				}				
			}
		}
		
		return checkPieces;
	}
	
	/**
	 * Determines if the Player is currently in check, given the opposing Player object
	 * to check against and returns the boolean result of that check.
	 * 
	 * @param them The opposing Player object used to determine if the Player is currently 
	 * in check.
	 * 
	 * @return true if any of the provided opposing Player object's ChessPieces are currently
	 * causing the Player check. Otherwise returns false.
	 */
	public boolean checkForCheck(Player them) {
		ArrayList<ChessPiece> checkPieces = this.piecesCausingCheck(them);

		if(checkPieces == null) {
			return false;
		}
		if(checkPieces.size() > 0) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Determines if the Player is currently in checkmate, given the opposing Player object
	 * to check against and returns the boolean result of that check.
	 * 
	 * @param them The opposing Player object used to determine if the Player is currently 
	 * in checkmate.
	 * 
	 * @return true if the Player is in check and has no valid move available to them that
	 * would result in them no longer being in check. Otherwise returns false.
	 */
	public boolean checkForCheckmate(Player them) {
		ArrayList<ChessPiece> checkPieces = this.piecesCausingCheck(them);

		// if we're not actually in check, we can't be in mate.
		if(checkPieces == null) {
			return false;
		} else {
			return this.checkForStalemate(them);
		}
	}
	
	/**
	 * Determines if the Players are currently in a stalemate, given the opposing Player object
	 * to check against and returns the boolean result of that check.
	 * 
	 * @param them The opposing Player object used to determine if the Players are currently 
	 * in a stalemate.
	 * 
	 * @return true if the Player is not in check but has no valid moves available to them. 
	 * Otherwise returns false.
	 */
	public boolean checkForStalemate(Player them) {		
		// loop through all my pieces
		for(int currentPieceIndex = 0; currentPieceIndex < myPieces.size(); currentPieceIndex++) {
			// loop through all possible spaces on the board
			for(int xPosToCheck = 0; xPosToCheck < 8; xPosToCheck++) {
				for(int yPosToCheck = 1; yPosToCheck < 9; yPosToCheck++) {
					// if my piece exists
					if(myPieces.get(currentPieceIndex) != null) {
						Player meSim = new Player(this);
						Player themSim = new Player(them);
						ChessPiece currentPiece = myPieces.get(currentPieceIndex);
							boolean pieceCanMoveHere = currentPiece.canMoveToNewPosition(xPosToCheck, yPosToCheck, ' ', this, them);
							int resultOfMove = meSim.actuallyHandleMove(themSim, xPosToCheck, yPosToCheck, currentPiece.xPos, currentPiece.yPos, " ", false);
							
							if(pieceCanMoveHere && (xPosToCheck != currentPiece.xPos || yPosToCheck != currentPiece.yPos) && (resultOfMove == Chess.newStateContinue) && !meSim.checkForCheck(themSim)) {
								return false;
							}							
					}
				}
			}
		}
		
		return true;
	}
	
	/**
	 * Actually performs the move defined by the provided parameters and returns the integer value
	 * representing the game state change caused by the move.
	 * 
	 * @param them The opposing Player object used to perform the move defined in the other parameters against.
	 * @param moveToFile The file we want to move our ChessPiece to.
	 * @param moveToRank The rank we want to move our ChessPiece to.
	 * @param moveFromFile The file the ChessPiece we want to move is located at currently.
	 * @param moveFromRank The rank the ChessPiece we want to move is located at currently.
	 * @param promotionCode The promotion code to use to promote the Pawn if it exists. This can be 
	 * any of the following: ROOK_CODE, KNIGHT_CODE, BISHOP_CODE, or QUEEN_CODE corresponding to 
	 * the ChessPiece child types of Rook, Knight, Bishop, and Queen respectively.
	 * @param logOutput The boolean that determines whether we print 'Check', 'Checkmate', 
	 * or 'Stalemate' to the console based on the occurrence of such states during this move.
	 * 
	 * @return The integer value associated with the result of the move defined by the parameters provided. This
	 * can be any of the values defined by the newStateCheck, newStateWin, newStateDraw, and newStateContinue members 
	 * defined on the Chess class. These values are defined based upon whether the current move has caused the opponent
	 * Player object provided in the them parameter to enter check, checkmate, stalemate, or none of the above respectively.  
	 */
	public int actuallyHandleMove(Player them, int moveToFile, int moveToRank, int moveFromFile, int moveFromRank, String promotionCode, boolean logOutput) {
		int xMagnitude = Math.abs(moveToFile - moveFromFile);
		int yMagnitude = Math.abs(moveToRank - moveFromRank);

		if(this.pieceAtPosition(moveFromFile, moveFromRank).canMoveToNewPosition(moveToFile, moveToRank, promotionCode.charAt(0), this, them)) {
			// move my piece at the starting x and y position to the destination x and y 
			this.pieceAtPosition(moveFromFile, moveFromRank).moveToPosition(moveToFile, moveToRank);
			
		}
		
		// if the opponent has a piece at our destination, take it
		// we can let this happen regardless of en passant / castling b/c
		// both of those cases will result in the space we're moving to being empty anyway,
		// so just doing this without checking either case does no harm.
		them.killPieceAtPosition(moveToFile, moveToRank);
		
		// handle en passant if necessary
		// if the piece I just moved is a pawn
		if(this.pieceAtPosition(moveToFile, moveToRank) instanceof Pawn) {
			int movementMultiplier = this.getMyKing().playerCode.equals(Player.WHITE_CODE) ? 1 : -1;
			ChessPiece enemyPiece = them.pieceAtPosition(moveToFile, moveToRank - movementMultiplier);

			// if the pawn is moving diagonally and the enemy piece we're moving past is a pawn
			if(xMagnitude == 1 && yMagnitude == 1 && enemyPiece != null && enemyPiece instanceof Pawn) {
				Pawn enemyPawn = (Pawn)enemyPiece;
				// if this enemy's pawn can be en passanted 
				if(enemyPawn.canBeEnPassanted) {
					them.killPieceAtPosition(moveToFile, moveToRank - movementMultiplier);
				}				
			}
		}
		
		// if castling, move the rook we're castling with appropriately.
		if(this.pieceAtPosition(moveToFile, moveToRank) instanceof King && xMagnitude == 2) {
			int castlingRookFile = moveToFile > moveFromFile ? 7 : 0;
			int castlingRookNewFile = moveToFile > moveFromFile ? 5 : 3;
			
			if(this.pieceAtPosition(castlingRookFile, moveToRank) != null) {
				this.pieceAtPosition(castlingRookFile, moveToRank).moveToPosition(castlingRookNewFile, moveToRank);				
			}
		}
		
		// promote piece if necessary
		ChessPiece myMovedPiece = this.pieceAtPosition(moveToFile, moveToRank);
		if(myMovedPiece instanceof Pawn) { 
			 this.promotePieceAtPosition(myMovedPiece.xPos, myMovedPiece.yPos, promotionCode);
		}
		
		// reset my en passant stuff
		this.clearEnPassantablePieces(myMovedPiece);
	
		if(logOutput) {
			// if we've put the opponent in check
			if(them.checkForCheck(this)) {
				if(them.checkForCheckmate(this)) {
					// check to see if we've put them in checkmate report so if we have
					// and update game state
					System.out.println("\nCheckmate\n");						

					return Chess.newStateWin;
				} else {
					// otherwise report that we're only in check and update game state
//					System.out.println("\nCheck");
					return Chess.newStateCheck;
				}
				// if we've not put our opponent in check
			} else {
				// check to see if we've reached a stalemate
				if(them.checkForStalemate(this)) {
					// if we have, report so and update game state
					System.out.println("\nStalemate");						
					
					return Chess.newStateDraw;
				}
			}
		}
		
		return Chess.newStateContinue;
	}
	
	/**
	 * Initializes the Player's myPieces and playerName
	 * as their starting values for the Black Player.
	 */
	public void initializeAsBlack() {
		//norm layout
		myPieces.add(new Rook  (0, 8, BLACK_CODE));
		myPieces.add(new Knight(1, 8, BLACK_CODE));
		myPieces.add(new Bishop(2, 8, BLACK_CODE));
		myPieces.add(new Queen (3, 8, BLACK_CODE));
		myPieces.add(new King  (4, 8, BLACK_CODE));
		myPieces.add(new Bishop(5, 8, BLACK_CODE));
		myPieces.add(new Knight(6, 8, BLACK_CODE));
		myPieces.add(new Rook  (7, 8, BLACK_CODE));
		
		myPieces.add(new Pawn  (0, 7, BLACK_CODE));
		myPieces.add(new Pawn  (1, 7, BLACK_CODE));
		myPieces.add(new Pawn  (2, 7, BLACK_CODE));
		myPieces.add(new Pawn  (3, 7, BLACK_CODE));
		myPieces.add(new Pawn  (4, 7, BLACK_CODE));
		myPieces.add(new Pawn  (5, 7, BLACK_CODE));
		myPieces.add(new Pawn  (6, 7, BLACK_CODE));
		myPieces.add(new Pawn  (7, 7, BLACK_CODE));
		
		
//		TA Feedback Case Setup
//		myPieces.add(new Rook  (0, 8, BLACK_CODE));
//		myPieces.add(new Knight(2, 6, BLACK_CODE));
//		myPieces.add(new Bishop(2, 8, BLACK_CODE));
//		myPieces.add(new Queen (3, 8, BLACK_CODE));
//		myPieces.add(new King  (4, 8, BLACK_CODE));
//		myPieces.add(new Bishop(5, 8, BLACK_CODE));
//		myPieces.add(new Knight(6, 8, BLACK_CODE));
//		myPieces.add(new Rook  (7, 8, BLACK_CODE));
//		
//		myPieces.add(new Pawn  (0, 6, BLACK_CODE));
//		myPieces.add(new Pawn  (1, 7, BLACK_CODE));
//		myPieces.add(new Pawn  (2, 7, BLACK_CODE));
//		myPieces.add(new Pawn  (3, 7, BLACK_CODE));
//		myPieces.add(new Pawn  (4, 5, BLACK_CODE));
//		myPieces.add(new Pawn  (5, 7, BLACK_CODE));
//		myPieces.add(new Pawn  (6, 7, BLACK_CODE));
//		myPieces.add(new Pawn  (7, 7, BLACK_CODE));
		
		// ez checkmate test
//		myPieces.add(new Rook  (0, 8, BLACK_CODE));
//		myPieces.add(new Knight(0, 6, BLACK_CODE));
//		myPieces.add(new Bishop(2, 8, BLACK_CODE));
//		myPieces.add(new King  (3, 8, BLACK_CODE));
//		myPieces.add(new Pawn  (0, 5, BLACK_CODE));
//		myPieces.add(new Pawn  (1, 7, BLACK_CODE));
//		myPieces.add(new Pawn  (2, 7, BLACK_CODE));
//		myPieces.add(new Pawn  (3, 7, BLACK_CODE));
//		myPieces.add(new Pawn  (5, 7, BLACK_CODE));
//		myPieces.add(new Pawn  (6, 7, BLACK_CODE));
//		myPieces.add(new Pawn  (7, 6, BLACK_CODE));
		
		//ez stalemate
//		myPieces.add(new King  (4, 8, BLACK_CODE));
//		myPieces.add(new Bishop(5, 8, BLACK_CODE));
//		myPieces.add(new Pawn  (0, 7, BLACK_CODE));
//		myPieces.add(new Pawn  (2, 7, BLACK_CODE));
//		myPieces.add(new Pawn  (4, 7, BLACK_CODE));
//		myPieces.add(new Pawn  (6, 7, BLACK_CODE));
		
		this.playerName = blackName;
		
		//test setup
//		myPieces.add(new King  (5, 8, BLACK_CODE));
	}

	/**
	 * Initializes the Player's myPieces and playerName
	 * as their starting values for the White Player.
	 */
	public void initializeAsWhite() {
		// norm layout
		myPieces.add(new Rook  (0, 1, WHITE_CODE));
		myPieces.add(new Knight(1, 1, WHITE_CODE));
		myPieces.add(new Bishop(2, 1, WHITE_CODE));
		myPieces.add(new Queen (3, 1, WHITE_CODE));
		myPieces.add(new King  (4, 1, WHITE_CODE));
		myPieces.add(new Bishop(5, 1, WHITE_CODE));
		myPieces.add(new Knight(6, 1, WHITE_CODE));
		myPieces.add(new Rook  (7, 1, WHITE_CODE));
		
		myPieces.add(new Pawn  (0, 2, WHITE_CODE));
		myPieces.add(new Pawn  (1, 2, WHITE_CODE));
		myPieces.add(new Pawn  (2, 2, WHITE_CODE));
		myPieces.add(new Pawn  (3, 2, WHITE_CODE));
		myPieces.add(new Pawn  (4, 2, WHITE_CODE));
		myPieces.add(new Pawn  (5, 2, WHITE_CODE));
		myPieces.add(new Pawn  (6, 2, WHITE_CODE));
		myPieces.add(new Pawn  (7, 2, WHITE_CODE));
		
		//ez stalemate
//		myPieces.add(new Rook  (0, 1, WHITE_CODE));
//		myPieces.add(new Knight(1, 1, WHITE_CODE));
//		myPieces.add(new Bishop(2, 1, WHITE_CODE));
//		myPieces.add(new Queen (3, 1, WHITE_CODE));
//		myPieces.add(new King  (4, 1, WHITE_CODE));
//		myPieces.add(new Bishop(5, 1, WHITE_CODE));
//		myPieces.add(new Knight(6, 1, WHITE_CODE));
//		myPieces.add(new Rook  (7, 1, WHITE_CODE));
//		myPieces.add(new Pawn  (0, 6, WHITE_CODE));
//		myPieces.add(new Pawn  (2, 6, WHITE_CODE));
//		myPieces.add(new Pawn  (4, 6, WHITE_CODE));
//		myPieces.add(new Pawn  (6, 6, WHITE_CODE));
		
		//ez checkmate test
//		myPieces.add(new Rook  (0, 1, WHITE_CODE));
//		myPieces.add(new Knight(1, 1, WHITE_CODE));
//		myPieces.add(new Bishop(2, 1, WHITE_CODE));
//		myPieces.add(new Queen (4, 5, WHITE_CODE));
//		myPieces.add(new King  (4, 1, WHITE_CODE));
//		myPieces.add(new Bishop(5, 1, WHITE_CODE));
//		myPieces.add(new Knight(6, 1, WHITE_CODE));
//		myPieces.add(new Rook  (4, 3, WHITE_CODE));
//		myPieces.add(new Pawn  (0, 2, WHITE_CODE));
//		myPieces.add(new Pawn  (1, 2, WHITE_CODE));
//		myPieces.add(new Pawn  (2, 2, WHITE_CODE));
//		myPieces.add(new Pawn  (3, 2, WHITE_CODE));
//		myPieces.add(new Pawn  (5, 2, WHITE_CODE));
//		myPieces.add(new Pawn  (6, 2, WHITE_CODE));
//		myPieces.add(new Pawn  (7, 4, WHITE_CODE));
		
//		TA Feedback Case Setup
//		myPieces.add(new Rook  (0, 1, WHITE_CODE));
//		myPieces.add(new Knight(1, 1, WHITE_CODE));
//		myPieces.add(new Bishop(2, 1, WHITE_CODE));
//		myPieces.add(new Queen (3, 1, WHITE_CODE));
//		myPieces.add(new King  (4, 1, WHITE_CODE));
//		myPieces.add(new Bishop(1, 5, WHITE_CODE));
//		myPieces.add(new Knight(5, 3, WHITE_CODE));
//		myPieces.add(new Rook  (7, 1, WHITE_CODE));
//		
//		myPieces.add(new Pawn  (0, 2, WHITE_CODE));
//		myPieces.add(new Pawn  (1, 2, WHITE_CODE));
//		myPieces.add(new Pawn  (3, 2, WHITE_CODE));
//		myPieces.add(new Pawn  (4, 4, WHITE_CODE));
//		myPieces.add(new Pawn  (5, 2, WHITE_CODE));
//		myPieces.add(new Pawn  (6, 2, WHITE_CODE));
//		myPieces.add(new Pawn  (7, 2, WHITE_CODE));
		
		this.playerName = whiteName;

		// test setup
//		myPieces.add(new King  (1, 6, WHITE_CODE));
//		myPieces.add(new Queen (2, 6, WHITE_CODE));
//		myPieces.add(new Rook  (6, 1, WHITE_CODE));
//		myPieces.add(new Rook  (4, 1, WHITE_CODE));
	}
	
	/**
	 * Determines if the Player has any ChessPieces in its myPieces collection at the provided
	 * x position and y position. If it does, return that piece, otherwise return null.
	 * 
	 * @param xPos The x position to check for pieces at.
	 * @param yPos The y position to check for pieces at.
	 * @return The ChessPiece object within the Player object's myPieces collection that is located
	 * at the position provided in the xPos and yPos parameters. If no piece exists at that location
	 * returns null.
	 */
	public ChessPiece pieceAtPosition(int xPos, int yPos) {
		
		for(int currentPieceIndex = 0; currentPieceIndex < myPieces.size(); currentPieceIndex++) {
			ChessPiece currentPiece = myPieces.get(currentPieceIndex);
			if(currentPiece != null) {
				if(currentPiece.isAtPosition(xPos, yPos)) {
					return currentPiece;
				}
			}
		}
		return null;
	}
	
	/**
	 * Tries to promote the Player object's Pawn object at the x position and y position provided in 
	 * the xPos and yPos parameters if that ChessPiece is a Pawn and is actually at the provided position.
	 * 
	 * @param xPos The x position to check for pieces at.
	 * @param yPos The y position to check for pieces at.
	 * @param promotionCode The promotion code to use to promote the Pawn if it exists. This can be 
	 * any of the following: ROOK_CODE, KNIGHT_CODE, BISHOP_CODE, or QUEEN_CODE corresponding to 
	 * the ChessPiece child types of Rook, Knight, Bishop, and Queen respectively.
	 */
	public void promotePieceAtPosition(int xPos, int yPos, String promotionCode) {
		for(int currentPieceIndex = 0; currentPieceIndex < myPieces.size(); currentPieceIndex++) {
			ChessPiece currentPiece = myPieces.get(currentPieceIndex);
			if(currentPiece != null) {
				if(currentPiece.isAtPosition(xPos, yPos) && currentPiece instanceof Pawn) {
					myPieces.set(currentPieceIndex, ((Pawn)currentPiece).promoteIfNecessary(promotionCode));
				}
			}
		}
	}

	public void undoPromotePieceAtPosition(int xPos, int yPos) {
		for(int currentPieceIndex = 0; currentPieceIndex < myPieces.size(); currentPieceIndex++) {
			ChessPiece currentPiece = myPieces.get(currentPieceIndex);
			if(currentPiece != null) {
				if(currentPiece.isAtPosition(xPos, yPos)) {
					myPieces.set(currentPieceIndex, currentPiece.undoPromotion());
				}
			}
		}
	}
	
	/**
	 * If we have a ChessPiece at the position defined by the xPos and yPos parameters provided
	 * set that piece to null, effectively "killing" it. 
	 * 
	 * @param xPos The x position to check for pieces at.
	 * @param yPos The y position to check for pieces at.
	 */
	public void killPieceAtPosition(int xPos, int yPos) {
		for(int currentPieceIndex = 0; currentPieceIndex < myPieces.size(); currentPieceIndex++) {
			ChessPiece currentPiece = myPieces.get(currentPieceIndex);
			if(currentPiece != null) {
				if(currentPiece.isAtPosition(xPos, yPos)) {
					myPieces.set(currentPieceIndex, null);
				}
			}
		}
	}
	
	/**
	 * Locates and returns the Player's King object.
	 * 
	 * @return the King object belonging to the Player object.
	 */
	public King getMyKing() {
		King myKing = null;
		for(int pieceIndex = 0; pieceIndex < myPieces.size(); pieceIndex++) {
			ChessPiece currentPiece = myPieces.get(pieceIndex);

			if(currentPiece instanceof King) {
				myKing = (King) currentPiece;
			}
		}
		
		return myKing;
	}

	public ChessMove getRandomMove(Player them) {
		ArrayList<ChessMove> validMoves = new ArrayList<ChessMove>();

		// loop through all my pieces
		for(int currentPieceIndex = 0; currentPieceIndex < myPieces.size(); currentPieceIndex++) {
			// loop through all possible spaces on the board
			for(int xPosToCheck = 0; xPosToCheck < 8; xPosToCheck++) {
				for(int yPosToCheck = 1; yPosToCheck < 9; yPosToCheck++) {
					// if my piece exists
					if(myPieces.get(currentPieceIndex) != null) {
						Player meSim = new Player(this);
						Player themSim = new Player(them);
						ChessPiece currentPiece = myPieces.get(currentPieceIndex);
						boolean pieceCanMoveHere = currentPiece.canMoveToNewPosition(xPosToCheck, yPosToCheck, ' ', this, them);
						int resultOfMove = meSim.actuallyHandleMove(themSim, xPosToCheck, yPosToCheck, currentPiece.xPos, currentPiece.yPos, " ", false);

						if(pieceCanMoveHere && (xPosToCheck != currentPiece.xPos || yPosToCheck != currentPiece.yPos) && (resultOfMove == Chess.newStateContinue) && !meSim.checkForCheck(themSim)) {
							ChessMove move = new ChessMove(this.playerCode, this.playerName);
							move.startPosition = "" + ChessPiece.parseIndexIntoFile(currentPiece.xPos) + Character.forDigit(currentPiece.yPos, 10);
							move.endPosition = "" + ChessPiece.parseIndexIntoFile(xPosToCheck) + Character.forDigit(yPosToCheck, 10);
							move.isPromotion = currentPiece.isPromotion(xPosToCheck, yPosToCheck, this, them);

							if(move.isPromotion) {
								move.takenPiece = them.pieceAtPosition(xPosToCheck, yPosToCheck);
							} else {
								move.isCastling = currentPiece.isCastling(xPosToCheck, yPosToCheck, this, them);

								if (move.isCastling) {
									move.castledRook = ((King) currentPiece).getCastlingTargetRook(xPosToCheck, yPosToCheck, this, them);
								}

								move.isEnPassant = currentPiece.isEnPassant(xPosToCheck, yPosToCheck, this, them);
								if (move.isEnPassant) {
									move.takenPiece = ((Pawn) currentPiece).getEnPassantTarget(xPosToCheck, yPosToCheck, this, them);
								} else {
									move.takenPiece = them.pieceAtPosition(xPosToCheck, yPosToCheck);
								}
							}

							move.output = resultOfMove;
							validMoves.add(move);
						}
					}
				}
			}
		}

		Random r = new Random();
		if(validMoves.size() > 0) {
			int randomMoveIndex = r.nextInt(validMoves.size());
			return validMoves.get(randomMoveIndex);
		} else {
			return null;
		}
	}
}
