package com.example.android73.model.chessPieces;

import com.example.android73.R;
import com.example.android73.model.Player;

import java.io.Serializable;

/**
 * This class extends ChessPiece and represents the Rook pieces on the Board.
 * It contains constructors to create a Rook object by providing an x position, a y position, and a playerCode
 * and to create a Rook object by providing another Rook object (to duplicate the provided Rook object).
 * This class also contains logic to determine whether a provided x and y position is valid for 
 * two given Player objects representing the player the Rook belongs to and that player's opponent
 * as well as logic to check whether the Rook can castle.
 * 
 * @author Rob Holda 
 * @author Nick LaBoy
 */
public class Rook extends ChessPiece implements Serializable {

	/**
	 * Initializes the Rook based on the x position, y position, and playerCode provided.
	 * 
	 * @param xPos The x position at which to create the Rook.
	 * @param yPos The y position at which to create the Rook.
	 * @param playerCode The playerCode representing the Player object the Rook belongs to.
	 */
	public Rook(int xPos, int yPos, String playerCode) {
			this.pieceCode = ROOK_CODE;
			this.playerCode = playerCode;
			this.xPos = xPos;
			this.yPos = yPos;

		if(playerCode == Player.WHITE_CODE) {
			pieceImageID = R.drawable.whiterook;
		} else {
			pieceImageID = R.drawable.blackrook;
		}
	}

	/**
	 * Initializes the Rook based on the provided Rook, essentially creating a 
	 * duplicate of the Rook passed in the rook argument.
	 * 
	 * @param rook The Rook object to duplicate.
	 */
	public Rook (Rook rook) {
		this.pieceCode = rook.pieceCode;
		this.playerCode = rook.playerCode;
		this.xPos = rook.xPos;
		this.yPos = rook.yPos;
		this.numTimesMoved = rook.numTimesMoved;
	}
	
	/**
	 * Determines whether the Rook can castle. This is currently based entirely upon the
	 * number of times it was previously moved (if the Rook has never been moved, it can
	 * castle.
	 * 
	 * @return true if the Rook has not been moved yet, otherwise false.
	 */
	public boolean canCastle() {
		return this.numTimesMoved == 0;
	}
	
	@Override
	public boolean canMoveToNewPosition(int xPosition, int yPosition, char promotionCode, Player me, Player them) {
		
		// if trying to move along the y-axis - check every position along it until the moveTo position
		if(xPosition == this.xPos) {
			int minY = Math.min(this.yPos, yPosition);
			int maxY = Math.max(this.yPos, yPosition);
			
			for(int yToCheck = minY; yToCheck <= maxY; yToCheck++ ) {
				// make sure there's no opposing piece at any position along our movement aside from
				// the destination (there can't be one where we start since our own piece is there
				// and if it's at the destination, we can just take the piece, so the move is still
				// valid).
				if(them.pieceAtPosition(xPosition, yToCheck) != null && yToCheck != yPosition) {
					return false;
				}
				// make sure we don't have a piece of our own at any position along our movement aside
				// from where we currently are.
				if(me.pieceAtPosition(xPosition, yToCheck) != null && yToCheck != this.yPos) {
					return false;
				}
			}			
		// if trying to move along the x-axis - check every position along it
		} else if (yPosition == this.yPos){
			int minX = Math.min(this.xPos, xPosition);
			int maxX = Math.max(this.xPos, xPosition);
			
			for(int xToCheck = minX; xToCheck <= maxX; xToCheck++ ) {
				// make sure there's no opposing piece at any position along our movement aside from
				// the destination (there can't be one where we start since our own piece is there
				// and if it's at the destination, we can just take the piece, so the move is still
				// valid).
				if(them.pieceAtPosition(xToCheck, yPosition) != null && xToCheck != xPosition) {
					return false;
				}
				// make sure we don't have a piece of our own at any position along our movement aside
				// from where we currently are.
				if(me.pieceAtPosition(xToCheck, yPosition) != null && xToCheck != this.xPos) {
					return false;
				}
			}
			
		// trying to move along both axes
		} else {
			return false;
		}
		
		return true;
	}

	public void undoCastling() {

		if(xPos == 3) {
			moveToPosition(0, this.yPos);
		} else {
			moveToPosition(7, this.yPos);
		}

		numTimesMoved = 0;
	}

	public boolean isEnPassant(int xPosition, int yPosition, Player me, Player them) {
		return false;
	}

	public boolean isCastling(int xPosition, int yPosition, Player me, Player them) {
		return false;
	}

	public boolean isPromotion(int xPosition, int yPosition, Player me, Player them) {
		return false;
	}

	@Override
	public Pawn undoPromotion() {
		Pawn oldSelf = new Pawn(this.xPos, this.yPos, this.playerCode);
		oldSelf.numTimesMoved = this.numTimesMoved;
		return oldSelf;
	}
}
