package com.example.android73.model.chessPieces;

import com.example.android73.R;
import com.example.android73.model.Player;

import java.io.Serializable;

/**
 * This class extends ChessPiece and represents the Bishop pieces on the Board.
 * It contains constructors to create a Bishop object by providing an x position, a y position, and a playerCode
 * and to create a Bishop object by providing another Bishop object (to duplicate the provided Bishop object).
 * This class also contains logic to determine whether a provided x and y position is valid for 
 * two given Player objects representing the player the Bishop belongs to and that player's opponent.
 * 
 * @author Rob Holda 
 * @author Nick LaBoy
 */

public class Bishop extends ChessPiece implements Serializable {
	
	/**
	 * Initializes the Bishop based on the x position, y position, and playerCode provided.
	 * 
	 * @param xPos The x position at which to create the Bishop.
	 * @param yPos The y position at which to create the Bishop.
	 * @param playerCode The playerCode representing the Player object the Bishop belongs to.
	 */
	public Bishop(int xPos, int yPos, String playerCode) {
		this.pieceCode = BISHOP_CODE;
		this.playerCode = playerCode;
		this.xPos = xPos;
		this.yPos = yPos;

		if(playerCode == Player.WHITE_CODE) {
			pieceImageID = R.drawable.whitebishop;
		} else {
			pieceImageID = R.drawable.blackbishop;
		}
}
	
	/**
	 * Initializes the Bishop based on the provided Bishop, essentially creating a 
	 * duplicate of the Bishop passed in the bishop argument.
	 * 
	 * @param bishop The Bishop object to duplicate.
	 */
	public Bishop(Bishop bishop) {
		this.pieceCode = bishop.pieceCode;
		this.playerCode = bishop.playerCode;
		this.xPos = bishop.xPos;
		this.yPos = bishop.yPos;
		this.numTimesMoved = bishop.numTimesMoved;
	}

	@Override
	public boolean canMoveToNewPosition(int xPosition, int yPosition, char promotionCode, Player me, Player them) {
		int minY = Math.min(this.yPos, yPosition);
		int maxY = Math.max(this.yPos, yPosition);
		int minX = Math.min(this.xPos, xPosition);
		int maxX = Math.max(this.xPos, xPosition);
		
		int magnitude = (maxY - minY);
		int xMultiplier = this.xPos > xPosition ? -1 : 1;
		int yMultiplier = this.yPos > yPosition ? -1 : 1;
		
		// if the attempted move doesn't have the same magnitude on x and y (and therefore it isn't
		// properly diagonal as far as we're concerned) the move should fail
		if((maxY - minY) != (maxX - minX)) {
			return false;
		}	
		
		for(int displacement = 0; displacement <= magnitude; displacement++) {
			int xToCheck = this.xPos + (xMultiplier * displacement);
			int yToCheck = this.yPos + (yMultiplier * displacement);

			// if enemy piece on position we're checking and that position isn't the destination
			// we're blocked and this move is invalid.
			if(them.pieceAtPosition(xToCheck, yToCheck) != null && (yToCheck != yPosition && xToCheck != xPosition)) {
				return false;
			}

			// if own piece on position we're checking and that position isn't our starting position
			// we're blocked and this move is invalid.
			if(me.pieceAtPosition(xToCheck, yToCheck) != null && (yToCheck != this.yPos && xToCheck != this.xPos)) {
				return false;
			}
		}
		
		return true;
	}

	public boolean isEnPassant(int xPosition, int yPosition, Player me, Player them) {
		return false;
	}

	public boolean isCastling(int xPosition, int yPosition, Player me, Player them) {
		return false;
	}

	public boolean isPromotion(int xPosition, int yPosition, Player me, Player them) {
		return false;
	}

	@Override
	public Pawn undoPromotion() {
		Pawn oldSelf = new Pawn(this.xPos, this.yPos, this.playerCode);
		oldSelf.numTimesMoved = this.numTimesMoved;
		return oldSelf;
	}
}
