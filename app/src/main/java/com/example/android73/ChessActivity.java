package com.example.android73;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android73.chess.Chess;
import com.example.android73.model.ChessMove;
import com.example.android73.model.Player;
import com.example.android73.model.chessPieces.ChessPiece;
import com.example.android73.model.chessPieces.King;
import com.example.android73.model.chessPieces.Pawn;
import com.example.android73.model.chessPieces.Rook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class ChessActivity extends AppCompatActivity implements View.OnClickListener, Serializable {
    public static int PLAY_MODE = 0;
    public static int REPLAY_MODE = 1;



    public ArrayList<ChessMove> moves;
    public Player white;
    public Player black;
    public ChessMove currentMove;
    public boolean canUndo;
    public boolean isGameOver;
    public int view_mode;
    public int currentMoveIndex;

    public void saveGameWithFilename(String filename) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(getFilesDir() + File.separator + filename + ".dat"));
            oos.writeObject(moves);
            oos.close();
        } catch(IOException e) {
            System.out.println("aaah");
            e.printStackTrace();
        }

        if(!filename.equalsIgnoreCase(MainActivity.currentGameFile)) {
            File dir = getFilesDir();
            File file = new File(dir, MainActivity.currentGameFile + ".dat");
            boolean deleted = file.delete();
            if(!deleted) {
                System.out.println("failed to delete obsolete save game...");
            }
        }
    }

    public void loadGameWithFilename(String filename) {
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(getFilesDir() + File.separator + filename));
            this.moves = (ArrayList<ChessMove>)ois.readObject();
            ois.close();
            System.out.println(this.moves);
        } catch(Exception e) {
            System.out.println("bbbh");
            e.printStackTrace();
        }
    }

    protected void initializeAsReplay() {
        Button drawButton = findViewById(R.id.drawButton);
        drawButton.setText("Prev Move");

        Button resignButton = findViewById(R.id.resignButton);
        resignButton.setText("Next Move");

        currentMoveIndex = 0;
        loadGameWithFilename(getIntent().getExtras().getString("replayFile"));
    }

    public void configureUndoButton() {
        Button undoButton = findViewById(R.id.undoButton);

        if(this.moves.size() > 0 && this.canUndo) {
            undoButton.setEnabled(true);
        } else {
            undoButton.setEnabled(false);
        }
    }

    public void configureSuggestButton() {
        Button suggestButton = findViewById(R.id.suggestButton);
        if(this.isGameOver || this.view_mode == REPLAY_MODE) {
            suggestButton.setEnabled(false);
        } else {
            suggestButton.setEnabled(true);
        }
    }

    public void configureNextAndPreviousButtons() {
        Button prevButton = findViewById(R.id.drawButton);
        Button nextButton = findViewById(R.id.resignButton);

        if(currentMoveIndex > 0) {
            prevButton.setEnabled(true);
        } else {
            prevButton.setEnabled(false);
        }

        if(currentMoveIndex < this.moves.size()) {
            nextButton.setEnabled(true);
        } else {
            nextButton.setEnabled(false);
        }
    }

    public void configureDrawButton() {
        Button drawButton = findViewById(R.id.drawButton);
        if(isGameOver) {
            drawButton.setEnabled(false);
        } else {
            drawButton.setEnabled(true);
        }
    }

    public void configureResignButton() {
        Button resignButton = findViewById(R.id.resignButton);
        if(isGameOver) {
            resignButton.setEnabled(false);
        } else {
            resignButton.setEnabled(true);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chess);
        moves = new ArrayList<ChessMove>();
        view_mode = getIntent().getExtras().getInt("mode");

        white = new Player(Player.WHITE_CODE);
        black = new Player(Player.BLACK_CODE);

        if(view_mode == PLAY_MODE) {
            isGameOver = false;
            String themName = "";
            if(getIntent().getExtras().getString("saveFile") != null) {
                loadGameWithFilename(getIntent().getExtras().getString("saveFile"));

                for(ChessMove move : moves) {
                    Player me;
                    Player them;

                    if(move.playerCode.equalsIgnoreCase(white.playerCode)) {
                        me = white;
                        them = black;
                    } else {
                        me = black;
                        them = white;
                    }
                    themName = them.playerName;

                    String promotionCode = move.promotionCode != null ? move.promotionCode : " ";
                    me.actuallyHandleMove(them, move.getEndFile(), move.getEndRank(), move.getStartFile(), move.getStartRank(), promotionCode, true);
                }
                setDisplayTextText(buildDisplayString(themName, false));
                renderPieces();
            } else {
                setDisplayTextText(buildDisplayString(white.playerName, false));
            }
        } else if(view_mode == REPLAY_MODE) {
            isGameOver = true;
            initializeAsReplay();
            configureNextAndPreviousButtons();
        }

        canUndo = true;
        configureUndoButton();
        configureSuggestButton();

        Button drawButton = findViewById(R.id.drawButton);
        drawButton.setOnClickListener(this);

        Button resignButton = findViewById(R.id.resignButton);
        resignButton.setOnClickListener(this);

        Button returnButton = findViewById(R.id.backToMenuButton);
        returnButton.setOnClickListener(this);

        Button undoButton = findViewById(R.id.undoButton);
        undoButton.setOnClickListener(this);

        if(view_mode == REPLAY_MODE) {
            undoButton.setEnabled(false);
        }

        Button suggestButton = findViewById(R.id.suggestButton);
        suggestButton.setOnClickListener(this);

        View a1 = findViewById(R.id.a1);
        a1.setOnClickListener(this);
        View a2 = findViewById(R.id.a2);
        a2.setOnClickListener(this);
        View a3 = findViewById(R.id.a3);
        a3.setOnClickListener(this);
        View a4 = findViewById(R.id.a4);
        a4.setOnClickListener(this);
        View a5 = findViewById(R.id.a5);
        a5.setOnClickListener(this);
        View a6 = findViewById(R.id.a6);
        a6.setOnClickListener(this);
        View a7 = findViewById(R.id.a7);
        a7.setOnClickListener(this);
        View a8 = findViewById(R.id.a8);
        a8.setOnClickListener(this);

        View b1 = findViewById(R.id.b1);
        b1.setOnClickListener(this);
        View b2 = findViewById(R.id.b2);
        b2.setOnClickListener(this);
        View b3 = findViewById(R.id.b3);
        b3.setOnClickListener(this);
        View b4 = findViewById(R.id.b4);
        b4.setOnClickListener(this);
        View b5 = findViewById(R.id.b5);
        b5.setOnClickListener(this);
        View b6 = findViewById(R.id.b6);
        b6.setOnClickListener(this);
        View b7 = findViewById(R.id.b7);
        b7.setOnClickListener(this);
        View b8 = findViewById(R.id.b8);
        b8.setOnClickListener(this);

        View c1 = findViewById(R.id.c1);
        c1.setOnClickListener(this);
        View c2 = findViewById(R.id.c2);
        c2.setOnClickListener(this);
        View c3 = findViewById(R.id.c3);
        c3.setOnClickListener(this);
        View c4 = findViewById(R.id.c4);
        c4.setOnClickListener(this);
        View c5 = findViewById(R.id.c5);
        c5.setOnClickListener(this);
        View c6 = findViewById(R.id.c6);
        c6.setOnClickListener(this);
        View c7 = findViewById(R.id.c7);
        c7.setOnClickListener(this);
        View c8 = findViewById(R.id.c8);
        c8.setOnClickListener(this);

        View d1 = findViewById(R.id.d1);
        d1.setOnClickListener(this);
        View d2 = findViewById(R.id.d2);
        d2.setOnClickListener(this);
        View d3 = findViewById(R.id.d3);
        d3.setOnClickListener(this);
        View d4 = findViewById(R.id.d4);
        d4.setOnClickListener(this);
        View d5 = findViewById(R.id.d5);
        d5.setOnClickListener(this);
        View d6 = findViewById(R.id.d6);
        d6.setOnClickListener(this);
        View d7 = findViewById(R.id.d7);
        d7.setOnClickListener(this);
        View d8 = findViewById(R.id.d8);
        d8.setOnClickListener(this);

        View e1 = findViewById(R.id.e1);
        e1.setOnClickListener(this);
        View e2 = findViewById(R.id.e2);
        e2.setOnClickListener(this);
        View e3 = findViewById(R.id.e3);
        e3.setOnClickListener(this);
        View e4 = findViewById(R.id.e4);
        e4.setOnClickListener(this);
        View e5 = findViewById(R.id.e5);
        e5.setOnClickListener(this);
        View e6 = findViewById(R.id.e6);
        e6.setOnClickListener(this);
        View e7 = findViewById(R.id.e7);
        e7.setOnClickListener(this);
        View e8 = findViewById(R.id.e8);
        e8.setOnClickListener(this);

        View f1 = findViewById(R.id.f1);
        f1.setOnClickListener(this);
        View f2 = findViewById(R.id.f2);
        f2.setOnClickListener(this);
        View f3 = findViewById(R.id.f3);
        f3.setOnClickListener(this);
        View f4 = findViewById(R.id.f4);
        f4.setOnClickListener(this);
        View f5 = findViewById(R.id.f5);
        f5.setOnClickListener(this);
        View f6 = findViewById(R.id.f6);
        f6.setOnClickListener(this);
        View f7 = findViewById(R.id.f7);
        f7.setOnClickListener(this);
        View f8 = findViewById(R.id.f8);
        f8.setOnClickListener(this);

        View g1 = findViewById(R.id.g1);
        g1.setOnClickListener(this);
        View g2 = findViewById(R.id.g2);
        g2.setOnClickListener(this);
        View g3 = findViewById(R.id.g3);
        g3.setOnClickListener(this);
        View g4 = findViewById(R.id.g4);
        g4.setOnClickListener(this);
        View g5 = findViewById(R.id.g5);
        g5.setOnClickListener(this);
        View g6 = findViewById(R.id.g6);
        g6.setOnClickListener(this);
        View g7 = findViewById(R.id.g7);
        g7.setOnClickListener(this);
        View g8 = findViewById(R.id.g8);
        g8.setOnClickListener(this);

        View h1 = findViewById(R.id.h1);
        h1.setOnClickListener(this);
        View h2 = findViewById(R.id.h2);
        h2.setOnClickListener(this);
        View h3 = findViewById(R.id.h3);
        h3.setOnClickListener(this);
        View h4 = findViewById(R.id.h4);
        h4.setOnClickListener(this);
        View h5 = findViewById(R.id.h5);
        h5.setOnClickListener(this);
        View h6 = findViewById(R.id.h6);
        h6.setOnClickListener(this);
        View h7 = findViewById(R.id.h7);
        h7.setOnClickListener(this);
        View h8 = findViewById(R.id.h8);
        h8.setOnClickListener(this);

        renderPieces();
    }

    public void renderPieceWithViewAndID(View imageView, int imageID) {
        ((ImageView)imageView).setImageResource(imageID);
    }

    public void renderPiecesWithPieceArray(ArrayList<ChessPiece> pieces) {
        ArrayList<ChessPiece> piecesToRender = pieces;
        for(ChessPiece currentPiece : piecesToRender) {
            if(currentPiece == null) {
                continue;
            }
            if(currentPiece.yPos == 1) {
                if(currentPiece.xPos == 0) {
                    renderPieceWithViewAndID(findViewById(R.id.a1), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 1) {
                    renderPieceWithViewAndID(findViewById(R.id.b1), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 2) {
                    renderPieceWithViewAndID(findViewById(R.id.c1), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 3) {
                    renderPieceWithViewAndID(findViewById(R.id.d1), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 4) {
                    renderPieceWithViewAndID(findViewById(R.id.e1), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 5) {
                    renderPieceWithViewAndID(findViewById(R.id.f1), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 6) {
                    renderPieceWithViewAndID(findViewById(R.id.g1), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 7) {
                    renderPieceWithViewAndID(findViewById(R.id.h1), currentPiece.pieceImageID);
                }
            } else if(currentPiece.yPos == 2) {
                if(currentPiece.xPos == 0) {
                    renderPieceWithViewAndID(findViewById(R.id.a2), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 1) {
                    renderPieceWithViewAndID(findViewById(R.id.b2), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 2) {
                    renderPieceWithViewAndID(findViewById(R.id.c2), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 3) {
                    renderPieceWithViewAndID(findViewById(R.id.d2), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 4) {
                    renderPieceWithViewAndID(findViewById(R.id.e2), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 5) {
                    renderPieceWithViewAndID(findViewById(R.id.f2), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 6) {
                    renderPieceWithViewAndID(findViewById(R.id.g2), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 7) {
                    renderPieceWithViewAndID(findViewById(R.id.h2), currentPiece.pieceImageID);
                }
            } else if(currentPiece.yPos == 3) {
                if(currentPiece.xPos == 0) {
                    renderPieceWithViewAndID(findViewById(R.id.a3), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 1) {
                    renderPieceWithViewAndID(findViewById(R.id.b3), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 2) {
                    renderPieceWithViewAndID(findViewById(R.id.c3), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 3) {
                    renderPieceWithViewAndID(findViewById(R.id.d3), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 4) {
                    renderPieceWithViewAndID(findViewById(R.id.e3), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 5) {
                    renderPieceWithViewAndID(findViewById(R.id.f3), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 6) {
                    renderPieceWithViewAndID(findViewById(R.id.g3), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 7) {
                    renderPieceWithViewAndID(findViewById(R.id.h3), currentPiece.pieceImageID);
                }
            } else if(currentPiece.yPos == 4) {
                if(currentPiece.xPos == 0) {
                    renderPieceWithViewAndID(findViewById(R.id.a4), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 1) {
                    renderPieceWithViewAndID(findViewById(R.id.b4), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 2) {
                    renderPieceWithViewAndID(findViewById(R.id.c4), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 3) {
                    renderPieceWithViewAndID(findViewById(R.id.d4), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 4) {
                    renderPieceWithViewAndID(findViewById(R.id.e4), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 5) {
                    renderPieceWithViewAndID(findViewById(R.id.f4), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 6) {
                    renderPieceWithViewAndID(findViewById(R.id.g4), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 7) {
                    renderPieceWithViewAndID(findViewById(R.id.h4), currentPiece.pieceImageID);
                }
            } else if(currentPiece.yPos == 5) {
                if(currentPiece.xPos == 0) {
                    renderPieceWithViewAndID(findViewById(R.id.a5), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 1) {
                    renderPieceWithViewAndID(findViewById(R.id.b5), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 2) {
                    renderPieceWithViewAndID(findViewById(R.id.c5), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 3) {
                    renderPieceWithViewAndID(findViewById(R.id.d5), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 4) {
                    renderPieceWithViewAndID(findViewById(R.id.e5), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 5) {
                    renderPieceWithViewAndID(findViewById(R.id.f5), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 6) {
                    renderPieceWithViewAndID(findViewById(R.id.g5), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 7) {
                    renderPieceWithViewAndID(findViewById(R.id.h5), currentPiece.pieceImageID);
                }
            } else if(currentPiece.yPos == 6) {
                if(currentPiece.xPos == 0) {
                    renderPieceWithViewAndID(findViewById(R.id.a6), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 1) {
                    renderPieceWithViewAndID(findViewById(R.id.b6), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 2) {
                    renderPieceWithViewAndID(findViewById(R.id.c6), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 3) {
                    renderPieceWithViewAndID(findViewById(R.id.d6), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 4) {
                    renderPieceWithViewAndID(findViewById(R.id.e6), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 5) {
                    renderPieceWithViewAndID(findViewById(R.id.f6), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 6) {
                    renderPieceWithViewAndID(findViewById(R.id.g6), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 7) {
                    renderPieceWithViewAndID(findViewById(R.id.h6), currentPiece.pieceImageID);
                }
            } else if(currentPiece.yPos == 7) {
                if(currentPiece.xPos == 0) {
                    renderPieceWithViewAndID(findViewById(R.id.a7), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 1) {
                    renderPieceWithViewAndID(findViewById(R.id.b7), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 2) {
                    renderPieceWithViewAndID(findViewById(R.id.c7), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 3) {
                    renderPieceWithViewAndID(findViewById(R.id.d7), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 4) {
                    renderPieceWithViewAndID(findViewById(R.id.e7), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 5) {
                    renderPieceWithViewAndID(findViewById(R.id.f7), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 6) {
                    renderPieceWithViewAndID(findViewById(R.id.g7), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 7) {
                    renderPieceWithViewAndID(findViewById(R.id.h7), currentPiece.pieceImageID);
                }
            } else if(currentPiece.yPos == 8) {
                if(currentPiece.xPos == 0) {
                    renderPieceWithViewAndID(findViewById(R.id.a8), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 1) {
                    renderPieceWithViewAndID(findViewById(R.id.b8), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 2) {
                    renderPieceWithViewAndID(findViewById(R.id.c8), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 3) {
                    renderPieceWithViewAndID(findViewById(R.id.d8), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 4) {
                    renderPieceWithViewAndID(findViewById(R.id.e8), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 5) {
                    renderPieceWithViewAndID(findViewById(R.id.f8), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 6) {
                    renderPieceWithViewAndID(findViewById(R.id.g8), currentPiece.pieceImageID);
                } else if(currentPiece.xPos == 7) {
                    renderPieceWithViewAndID(findViewById(R.id.h8), currentPiece.pieceImageID);
                }
            }
        }
    }

    public void clearBoardImages() {
        ((ImageView)findViewById(R.id.a1)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.a2)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.a3)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.a4)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.a5)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.a6)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.a7)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.a8)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.b1)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.b2)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.b3)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.b4)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.b5)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.b6)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.b7)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.b8)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.c1)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.c2)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.c3)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.c4)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.c5)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.c6)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.c7)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.c8)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.d1)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.d2)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.d3)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.d4)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.d5)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.d6)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.d7)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.d8)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.e1)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.e2)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.e3)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.e4)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.e5)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.e6)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.e7)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.e8)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.f1)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.f2)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.f3)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.f4)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.f5)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.f6)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.f7)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.f8)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.g1)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.g2)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.g3)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.g4)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.g5)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.g6)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.g7)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.g8)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.h1)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.h2)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.h3)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.h4)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.h5)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.h6)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.h7)).setImageBitmap(null);
        ((ImageView)findViewById(R.id.h8)).setImageBitmap(null);
    }
    public void renderPieces() {
        clearBoardImages();
        renderPiecesWithPieceArray(white.myPieces);
        renderPiecesWithPieceArray(black.myPieces);
    }

    public void displaySnackBar(String message) {
        if(message.length() > 0) {
            Button returnButton = findViewById(R.id.backToMenuButton);
            Snackbar.make(returnButton, message, Snackbar.LENGTH_SHORT).setAction("Action", null).show();
        }
    }

    public boolean wasDrawOffered(boolean undoing) {
        ChessMove moveToUse = null;
        if(view_mode == REPLAY_MODE) {
            if(undoing) {
                moveToUse = moves.get(currentMoveIndex - 1);
            } else {
                moveToUse = moves.get(currentMoveIndex);
            }
        } else {
            if (moves.size() > 0) {
                moveToUse = moves.get(moves.size() - 1);
            }
        }

        return moveToUse.playerTappedDraw;
    }

    public void setDisplayTextText(String textToDisplay) {
        TextView displayText = findViewById(R.id.outputTextView);
        displayText.setText(textToDisplay);
    }

    public void handlePromotion() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Please choose a piece to promote your pawn to...");
        builder.setItems(new CharSequence[]
                        {"Queen", "Rook", "Bishop", "Knight"},
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        ChessMove moveToAdjust = ChessActivity.this.currentMove;
                        Player me;
                        Player them;
                        String playerName;
                        moveToAdjust.promotionCode = " ";

                        if(moveToAdjust.playerCode.equals(Player.WHITE_CODE)) {
                            me = white;
                            them = black;
                        } else {
                            me = black;
                            them = white;
                        }

                        switch (which) {
                            case 0:
                                moveToAdjust.promotionCode = ChessPiece.QUEEN_CODE;
                                break;
                            case 1:
                                moveToAdjust.promotionCode = ChessPiece.ROOK_CODE;
                                break;
                            case 2:
                                moveToAdjust.promotionCode = ChessPiece.BISHOP_CODE;
                                break;
                            case 3:
                                moveToAdjust.promotionCode = ChessPiece.KNIGHT_CODE;
                                break;
                        }
                        moveToAdjust.output = me.actuallyHandleMove(them, moveToAdjust.getEndFile(), moveToAdjust.getEndRank(), moveToAdjust.getStartFile(), moveToAdjust.getStartRank(), moveToAdjust.promotionCode, true);
                        ChessActivity.this.moves.add(moveToAdjust);
                        saveGameWithFilename(MainActivity.currentGameFile);
                        ChessActivity.this.currentMove = new ChessMove(them.playerCode, them.playerName);
                        configureUndoButton();
                        updateGameState();
                        configureSuggestButton();
                        configureDrawButton();
                        configureResignButton();
                        renderPieces();

                        if(moveToAdjust.output == ChessMove.NO_OUTPUT || moveToAdjust.output == ChessMove.CHECK_OUTPUT) {
                            playerName = them.playerName;
                        } else {
                            playerName = me.playerName;
                        }

                        setDisplayTextText(buildDisplayString(playerName, false));
                        displaySnackBar(moveToAdjust.toOutputString());
                        configureSuggestButton();

                        if(isGameOver) {
                           promptForSaveGame();
                        }
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public String buildDisplayString(String playerName, boolean undoing) {
        String displayString = "";
        ChessMove moveToUse = null;
        if(view_mode == REPLAY_MODE) {
            if(undoing) {
                moveToUse = moves.get(currentMoveIndex - 1);
            } else {
                moveToUse = moves.get(currentMoveIndex);
            }
        } else {
            if (moves.size() > 0) {
                moveToUse = moves.get(moves.size() - 1);
            }
        }


        if (moves.size() > 0) {
            if(moveToUse.shouldPrintMove()) {
                displayString = playerName + "'s Turn";
            }

            String displaySubstring = moveToUse.toOutputString();
            if (displaySubstring.length() > 0) {
                if(displayString.length() > 0) {
                    displayString = displayString + " - " + displaySubstring;
                } else {
                    displayString = displaySubstring;
                }

            }
        } else {
            return playerName + "'s Turn";
        }

        String drawString = wasDrawOffered(undoing) ? " (Draw Offered)" : "";

        if(moveToUse.shouldPrintMove()) {
            return displayString + drawString;
        } else {
            return displayString;
        }
    }

    @Override
    public void onClick(View v) {

        if(!isGameOver) {
            if(     v.getId() == R.id.a1 || v.getId() == R.id.a2 || v.getId() == R.id.a3 || v.getId() == R.id.a4 || v.getId() == R.id.a5 || v.getId() == R.id.a6 || v.getId() == R.id.a7 || v.getId() == R.id.a8 ||
                    v.getId() == R.id.b1 || v.getId() == R.id.b2 || v.getId() == R.id.b3 || v.getId() == R.id.b4 || v.getId() == R.id.b5 || v.getId() == R.id.b6 || v.getId() == R.id.b7 || v.getId() == R.id.b8 ||
                    v.getId() == R.id.c1 || v.getId() == R.id.c2 || v.getId() == R.id.c3 || v.getId() == R.id.c4 || v.getId() == R.id.c5 || v.getId() == R.id.c6 || v.getId() == R.id.c7 || v.getId() == R.id.c8 ||
                    v.getId() == R.id.d1 || v.getId() == R.id.d2 || v.getId() == R.id.d3 || v.getId() == R.id.d4 || v.getId() == R.id.d5 || v.getId() == R.id.d6 || v.getId() == R.id.d7 || v.getId() == R.id.d8 ||
                    v.getId() == R.id.e1 || v.getId() == R.id.e2 || v.getId() == R.id.e3 || v.getId() == R.id.e4 || v.getId() == R.id.e5 || v.getId() == R.id.e6 || v.getId() == R.id.e7 || v.getId() == R.id.e8 ||
                    v.getId() == R.id.f1 || v.getId() == R.id.f2 || v.getId() == R.id.f3 || v.getId() == R.id.f4 || v.getId() == R.id.f5 || v.getId() == R.id.f6 || v.getId() == R.id.f7 || v.getId() == R.id.f8 ||
                    v.getId() == R.id.g1 || v.getId() == R.id.g2 || v.getId() == R.id.g3 || v.getId() == R.id.g4 || v.getId() == R.id.g5 || v.getId() == R.id.g6 || v.getId() == R.id.g7 || v.getId() == R.id.g8 ||
                    v.getId() == R.id.h1 || v.getId() == R.id.h2 || v.getId() == R.id.h3 || v.getId() == R.id.h4 || v.getId() == R.id.h5 || v.getId() == R.id.h6 || v.getId() == R.id.h7 || v.getId() == R.id.h8) {
                if(this.currentMove == null || this.currentMove.endPosition != null) {
                    if(this.moves.size() == 0) {
                        this.currentMove = new ChessMove(Player.WHITE_CODE, Player.whiteName);
                    } else {
                        System.out.println("hey" + this.moves.get(this.moves.size() - 1).playerCode);
                        if(this.moves.get(this.moves.size() - 1).playerCode.equalsIgnoreCase(Player.WHITE_CODE)) {
                            this.currentMove = new ChessMove(Player.BLACK_CODE, Player.blackName);
                        } else {
                            this.currentMove = new ChessMove(Player.WHITE_CODE, Player.whiteName);
                        }
                    }
                }

                if(this.currentMove.startPosition == null) {
                    this.currentMove.startPosition = (String)v.getTag();
                } else {
                    this.currentMove.endPosition = (String) v.getTag();

                    Player me;
                    Player them;

                    if (this.currentMove.playerName.equalsIgnoreCase(white.playerName)) {
                        me = white;
                        them = black;
                    } else {
                        me = black;
                        them = white;
                    }

                    if (Chess.checkMoveWithValidatedInput(this.currentMove.toString(), me, them)) {
                        ChessPiece myPiece = me.pieceAtPosition(this.currentMove.getStartFile(), this.currentMove.getStartRank());

                        currentMove.isPromotion = myPiece.isPromotion(currentMove.getEndFile(), currentMove.getEndRank(), me, them);
                        if (currentMove.isPromotion) {
                            currentMove.takenPiece = them.pieceAtPosition(currentMove.getEndFile(), currentMove.getEndRank());
                            handlePromotion();
                        } else {
                            currentMove.isCastling = myPiece.isCastling(currentMove.getEndFile(), currentMove.getEndRank(), me, them);
                            if (currentMove.isCastling) {
                                currentMove.castledRook = ((King) myPiece).getCastlingTargetRook(currentMove.getEndFile(), currentMove.getEndRank(), me, them);
                            }

                            currentMove.isEnPassant = myPiece.isEnPassant(currentMove.getEndFile(), currentMove.getEndRank(), me, them);
                            if (currentMove.isEnPassant) {
                                currentMove.takenPiece = ((Pawn) myPiece).getEnPassantTarget(currentMove.getEndFile(), currentMove.getEndRank(), me, them);
                            } else {
                                currentMove.takenPiece = them.pieceAtPosition(currentMove.getEndFile(), currentMove.getEndRank());
                            }

                            this.currentMove.output = me.actuallyHandleMove(them, this.currentMove.getEndFile(), this.currentMove.getEndRank(), this.currentMove.getStartFile(), this.currentMove.getStartRank(), " ", true);
                            this.moves.add(this.currentMove);
                            saveGameWithFilename(MainActivity.currentGameFile);
                            displaySnackBar(this.currentMove.toOutputString());
                            setDisplayTextText(buildDisplayString(them.playerName, false));
                            this.currentMove = new ChessMove(them.playerCode, them.playerName);
                        }

                        canUndo = true;
                        this.renderPieces();
                    } else {
                        this.currentMove.startPosition = null;
                        this.currentMove.endPosition = null;
                        displaySnackBar("Invalid move!");
                    }
                }

                configureUndoButton();
                updateGameState();
                if(this.view_mode == PLAY_MODE && this.isGameOver) {
                    this.promptForSaveGame();
                }
            }
        }

        if(v.getId() == R.id.backToMenuButton) {
            finish();
        }

        if(v.getId() == R.id.suggestButton) {
            Player me;
            Player them;
            if (moves.size() == 0 || moves.get(moves.size() - 1).playerCode.equalsIgnoreCase(Player.BLACK_CODE)) {
                me = white;
                them = black;
            } else {
                me = black;
                them = white;
            }
            ChessMove randomMove = me.getRandomMove(them);

            randomMove.output = me.actuallyHandleMove(them, randomMove.getEndFile(), randomMove.getEndRank(), randomMove.getStartFile(), randomMove.getStartRank(), " ", true);

            moves.add(randomMove);
            saveGameWithFilename(MainActivity.currentGameFile);
            canUndo = true;
            updateGameState();
            configureUndoButton();
            configureSuggestButton();
            configureDrawButton();
            configureResignButton();
            displaySnackBar(moves.get(moves.size() - 1).toOutputString());
            setDisplayTextText(buildDisplayString(them.playerName, false));
            this.currentMove = new ChessMove(them.playerCode, them.playerName);
            renderPieces();
        }

        if(v.getId() == R.id.undoButton) {
            if(canUndo) {
                if(moves.size() > 0) {
                    if(moves.size() >= 2) {
                        if(moves.get(moves.size() - 1).playerTappedDraw && moves.get(moves.size() - 2).playerTappedDraw) {

                            Player them;
                            Player me;

                            if(moves.get(moves.size() - 1).playerCode.equals(Player.WHITE_CODE)) {
                                them = black;
                                me = white;
                            } else {
                                them = white;
                                me = black;
                            }

                            moves.remove(moves.size() - 1);
                            saveGameWithFilename(MainActivity.currentGameFile);

                            this.currentMove = new ChessMove(me.playerCode, me.playerName);

                            canUndo = false;
                            isGameOver = false;
                            configureUndoButton();
                            configureDrawButton();
                            configureResignButton();
                            configureSuggestButton();


                            setDisplayTextText(buildDisplayString(me.playerName, false));
                            renderPieces();
                            if(moves.size() > 0) {
                                displaySnackBar(moves.get(moves.size() - 1).toOutputString());
                            }
                            return;
                        }
                    }

                    ChessMove moveToUndo = moves.get(moves.size() - 1);
                    if(moveToUndo.output != ChessMove.RESIGNATION_OUTPUT) {
                        Player me;
                        Player them;

                        if(moveToUndo.playerCode.equals(Player.WHITE_CODE)) {
                            me = white;
                            them = black;
                        } else {
                            me = black;
                            them = white;
                        }

                        if(moveToUndo.isCastling) {
                            ((Rook)moveToUndo.castledRook).undoCastling();
                        }

                        if(moveToUndo.isPromotion) {
                            me.undoPromotePieceAtPosition(moveToUndo.getEndFile(), moveToUndo.getEndRank());
                        }

                    me.pieceAtPosition(moveToUndo.getEndFile(), moveToUndo.getEndRank()).moveToPosition(moveToUndo.getStartFile(), moveToUndo.getStartRank());
                    me.pieceAtPosition(moveToUndo.getStartFile(), moveToUndo.getStartRank()).numTimesMoved-=2;
                    them.myPieces.add(moveToUndo.takenPiece);
                    }

                    Player them;
                    Player me;

                    if(moveToUndo.playerCode.equals(Player.WHITE_CODE)) {
                        them = black;
                        me = white;
                    } else {
                        them = white;
                        me = black;
                    }

                    moves.remove(moves.size() - 1);
                    saveGameWithFilename(MainActivity.currentGameFile);
                    this.currentMove = new ChessMove(me.playerCode, me.playerName);
                    canUndo = false;
                    configureUndoButton();

                    setDisplayTextText(buildDisplayString(moveToUndo.playerName, false));
                    renderPieces();
                    if(moves.size() > 0) {
                        displaySnackBar(moves.get(moves.size() - 1).toOutputString());
                    }

                    this.isGameOver = false;
                    renderPieces();
                } else {
                    displaySnackBar("No move to undo!");
                }
            } else {
                displaySnackBar("Cannot undo more than one move at a time!");
            }
        }

        if(this.view_mode == PLAY_MODE) {
            if(v.getId() == R.id.resignButton) {
                Player me;
                Player them;

                if(this.moves.size() == 0 || this.moves.get(this.moves.size() - 1).playerCode.equals(Player.BLACK_CODE)) {
                    me = white;
                    them = black;
                } else {
                    me = black;
                    them = white;
                }

                ChessMove resignMove = new ChessMove(me.playerCode, me.playerName);
                resignMove.output = ChessMove.RESIGNATION_OUTPUT;
                this.moves.add(resignMove);
                saveGameWithFilename(MainActivity.currentGameFile);
                this.currentMove = new ChessMove(them.playerCode, them.playerName);

                isGameOver = true;
                canUndo = true;
                setDisplayTextText(this.moves.get(this.moves.size() - 1).toOutputString());
                displaySnackBar(this.moves.get(this.moves.size() - 1).toOutputString());
                configureUndoButton();
                promptForSaveGame();
            }

            if(v.getId() == R.id.drawButton) {
                //if this isn't the first move and the previous move included offering draw. End the game in a draw.
                if(this.moves.size() > 0 && this.moves.get(this.moves.size() - 1).playerTappedDraw) {
                    Player me;
                    Player them;
                    if(this.moves.get(this.moves.size() - 1).playerCode.equalsIgnoreCase(Player.WHITE_CODE)) {
                        me = black;
                        them = white;
                    } else {
                        me = white;
                        them = black;
                    }

                    isGameOver = true;
                    canUndo = true;
                    ChessMove resignMove = new ChessMove(me.playerCode, me.playerName);
                    resignMove.playerTappedDraw = true;
                    resignMove.output = ChessMove.DRAW_OUTPUT;
                    this.moves.add(resignMove);
                    saveGameWithFilename(MainActivity.currentGameFile);
                    setDisplayTextText(resignMove.toOutputString());
                    this.currentMove = new ChessMove(them.playerCode, them.playerName);
                    promptForSaveGame();
                }

                //if this is the first move or the previous move was -not- offering draw.
                if(this.moves.size() == 0 || !this.moves.get(this.moves.size() - 1).playerTappedDraw) {
                    //if there isn't a current move
                    if(this.currentMove == null) {
                        //if this is the first move create a new move with white as the person moving
                        if(this.moves.size() == 0) {
                            this.currentMove = new ChessMove(Player.WHITE_CODE, Player.whiteName);
                        }
                        // otherwise create a new move with the current player (opposite player of the last move completed)
                        else {
                            if(this.moves.get(this.moves.size() - 1).playerCode == Player.WHITE_CODE) {
                                this.currentMove = new ChessMove(Player.BLACK_CODE, Player.blackName);
                            } else {
                                this.currentMove = new ChessMove(Player.WHITE_CODE, Player.whiteName);
                            }
                        }

                        // in either case, set that move to offering draw
                        this.currentMove.playerTappedDraw = true;
                    } else {
                        this.currentMove.playerTappedDraw = true;
                    }
                }

                configureUndoButton();
                configureSuggestButton();
                configureDrawButton();
                configureResignButton();
            }
        } else {
            // prev move
            if(v.getId() == R.id.drawButton) {
//                if(moves.size() >= 2) {
//                    if(moves.get(moves.size() - 1).playerTappedDraw && moves.get(moves.size() - 2).playerTappedDraw) {
//
//                        Player them;
//                        Player me;
//
//                        if(moves.get(moves.size() - 1).playerCode.equals(Player.WHITE_CODE)) {
//                            them = black;
//                            me = white;
//                        } else {
//                            them = white;
//                            me = black;
//                        }
//
//                        moves.remove(moves.size() - 1);
//                        saveGameWithFilename(MainActivity.currentGameFile);
//
//                        this.currentMove = new ChessMove(me.playerCode, me.playerName);
//
//                        canUndo = false;
//                        isGameOver = false;
//                        configureUndoButton();
//                        configureDrawButton();
//                        configureResignButton();
//                        configureSuggestButton();
//
//
//                        setDisplayTextText(buildDisplayString(me.playerName));
//                        renderPieces();
//                        if(moves.size() > 0) {
//                            displaySnackBar(moves.get(moves.size() - 1).toOutputString());
//                        }
//                        return;
//                    }
//                }

                ChessMove moveToUndo = moves.get(currentMoveIndex - 1);
                if(moveToUndo.output != ChessMove.RESIGNATION_OUTPUT && moveToUndo.output != ChessMove.DRAW_OUTPUT) {
                    Player me;
                    Player them;

                    if(moveToUndo.playerCode.equals(Player.WHITE_CODE)) {
                        me = white;
                        them = black;
                    } else {
                        me = black;
                        them = white;
                    }

                    if(moveToUndo.isCastling) {
                        ((Rook)me.pieceAtPosition(moveToUndo.castledRook.xPos, moveToUndo.castledRook.yPos)).undoCastling();
                    }

                    if(moveToUndo.isPromotion) {
                        me.undoPromotePieceAtPosition(moveToUndo.getEndFile(), moveToUndo.getEndRank());
                    }

                    me.pieceAtPosition(moveToUndo.getEndFile(), moveToUndo.getEndRank()).moveToPosition(moveToUndo.getStartFile(), moveToUndo.getStartRank());
                    me.pieceAtPosition(moveToUndo.getStartFile(), moveToUndo.getStartRank()).numTimesMoved-=2;
                    them.myPieces.add(moveToUndo.takenPiece);
                }

                Player them;
                Player me;

                if(moveToUndo.playerCode.equals(Player.WHITE_CODE)) {
                    them = black;
                    me = white;
                } else {
                    them = white;
                    me = black;
                }

                currentMoveIndex--;

                if(currentMoveIndex > 0) {
                    displaySnackBar(this.moves.get(currentMoveIndex - 1).toOutputString());
                    setDisplayTextText(buildDisplayString(me.playerName, true));
                } else {
                    setDisplayTextText(buildDisplayString(white.playerName, false));
                }

                renderPieces();
            }

            // next move
            if(v.getId() == R.id.resignButton) {
                ChessMove thisMove = this.moves.get(currentMoveIndex);
                Player me;
                Player them;

                if(thisMove.playerCode.equalsIgnoreCase(Player.WHITE_CODE)) {
                    me = white;
                    them = black;
                } else {
                    me = black;
                    them = white;
                }

                if(thisMove.output == ChessMove.RESIGNATION_OUTPUT || thisMove.output == ChessMove.DRAW_OUTPUT) {
                    renderPieces();
                    displaySnackBar(thisMove.toOutputString());
                    setDisplayTextText(buildDisplayString(them.playerName, false));
                    currentMoveIndex++;
                    configureNextAndPreviousButtons();
                    return;
                }

                String promotionCode = thisMove.promotionCode != null ? thisMove.promotionCode : " ";
                me.actuallyHandleMove(them, thisMove.getEndFile(), thisMove.getEndRank(), thisMove.getStartFile(), thisMove.getStartRank(), promotionCode, true);
                renderPieces();
                displaySnackBar(thisMove.toOutputString());
                setDisplayTextText(buildDisplayString(them.playerName, false));
                currentMoveIndex++;
            }
        }

        if(this.view_mode == REPLAY_MODE) {
            configureNextAndPreviousButtons();
        } else if(this.view_mode == PLAY_MODE) {

            updateGameState();
            configureSuggestButton();
            configureDrawButton();
            configureResignButton();
        }
    }

    public void promptForSaveGame() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Save Game?");
        builder.setMessage("Please enter a title and tap 'Save' to save your game, or tap 'Don't Save' to exit this prompt without saving.");

        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                saveGameWithFilename(input.getText().toString());
            }
        });
        builder.setNegativeButton("Don't Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                File dir = getFilesDir();
                File file = new File(dir, MainActivity.currentGameFile + ".dat");
                boolean deleted = file.delete();
                if(!deleted) {
                    System.out.println("failed to delete obsolete save game...");
                }
                dialog.cancel();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public void updateGameState() {
        if(this.moves.size() > 0) {
            if(this.moves.get(this.moves.size() - 1).output == ChessMove.CHECKMATE_OUTPUT ||
                    this.moves.get(this.moves.size() - 1).output == ChessMove.STALEMATE_OUTPUT ||
                    this.moves.get(this.moves.size() - 1).output == ChessMove.RESIGNATION_OUTPUT ||
                    this.moves.get(this.moves.size() - 1).output == ChessMove.DRAW_OUTPUT) {
                this.isGameOver = true;
            }
        }
    }
}
