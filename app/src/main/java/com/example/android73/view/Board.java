package com.example.android73.view;

import com.example.android73.model.Player;
import com.example.android73.model.chessPieces.ChessPiece;

/**
 * The Board class handles printing the current state 
 * of the game to the screen given two Player objects.
 * 
 * @author Rob Holda 
 * @author Nick LaBoy
 *
 */
public class Board {
	/*** The defined width of the board.*/
	public int boardWidth = 8;
	/*** The defined height of the board. This is actually one more
	 * than the board appears because there is a hidden rank 0 which
	 * is never a valid move and is not rendered. This is defined this 
	 * way because arrays start at 0 and the ranks displayed on screen
	 * start at 1.*/
	public int boardHeight = 9;

	/**
	 * Draw the current board state using the Player objects provided in 
	 * the white and black parameters.
	 * 
	 * @param white The first Player object to use in drawing the board.
	 * @param black The second Player object to use in drawing the board.
	 */
	public void drawBoard(Player white, Player black) {
		for(int yPos = boardHeight - 1; yPos > 0; yPos--) {
			String currentRowString = "";
			for(int xPos = 0; xPos < boardWidth; xPos++) {
				ChessPiece cp = white.pieceAtPosition(xPos, yPos);
				if(cp == null) {
					cp = black.pieceAtPosition(xPos, yPos);
				}
				
				if(cp == null) {
					if((yPos - xPos) % 2 == 0) {
						currentRowString += "  ";
					} else {
						currentRowString += "##";
					}					
				} else {
					currentRowString += cp.toString();
				}

				currentRowString += " ";
			}
			currentRowString += yPos;
			System.out.println(currentRowString);
		}
		System.out.println(" a  b  c  d  e  f  g  h");
		System.out.println("");
	}
}
